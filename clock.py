import time
import pypcd8544 as p

p.begin(20,False)
p.setTextColor(True,False)	## textcolor, textbgcolor

print "Ctrl-C to exit"
loop = True
while loop:
	try:
		clk = time.strftime("%H:%M:%S")
		p.write(clk)
		p.display()
		time.sleep(0.1)
		p.clearDisplay()
	except KeyboardInterrupt:
		print "Exit.."
		p.GPIO.cleanup()
		loop = False
		
